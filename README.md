In modern world, people want everything to be lightweight and portable. Gone are the days when individuals had to stick to their gaming desktop so that they can run the high end games and play only when they are home. Today, as with the work laptops, you have the gaming laptops too. When you will google for the best gaming laptop reviews, you will find ASUS ROG STRIX GL502VM in almost all the lists and that too among the top in the rankings. So, what is all the fuss about this laptop by ASUS? We will find it out below.

***Features of ASUS ROG STRIX GL502VM***

The ASUS GL502 is labelled as the [best gaming laptop](https://www.10awesomegears.com/best-gaming-laptop/) for a number of reasons. This model comes with 2.6 GHz core i7 processor that can reach the speed of up to 3.5 GHz in turbo boost. There is Nvidia GTX 1070 that has 8GB GDDRS RAM alongside an Intel HD Graphics 530 model. The RAM is 16 GB DDR4 while for storage you get an SSD and an HDD of 256 GB and 1 TB respectively. Moving ahead, there is a 15.6 inches FHD display on IPS panel that has G-Sync. For connectivity, all standard ports are available to you. 



***How it looks?***

The company is trying to take their consumers away from the traditional black and red color scheme that you get in most of the gaming laptops. However, the keyboard backlighting is still red while there is a new paintjob. Many people who are looking at it from the G501 perspective may note that this one is a standout design on its own rather than its predecessor which was pretty much identical to the MacBook Pro. 


***Cooling is covered***

Most of the manufacturers that are trying to make thin and lightweight gaming laptops are compromising on a few features which usually are associated with its cooling. As a result, there are increasing cases of burnout where a particular component is blown. However, ASUS got it covered in this ROG machine because the cooling is completely taken care of as there are copper heat pipes with cooling fans to work on GPU and CPU separately.

***Looking at the performance***

If you wish to play high end games and that too on a high graphics setting with the desire that your laptop does not lag or the graphics does not appear distorted, this might be your go to machine. You can run the games at 60 FPS with high to ultrahigh settings and the machine would work fine as ever, providing you the best gaming experience, which is one of the reasons why it is the best gaming laptop available to you in the market.

***Final words***

Overall, in terms of performance, build, portability, and design, the ASUS ROG STRIX GL502VM appears to be a great laptop. However, it does lack in battery as the timing is not very good which is why you might want to be near a socket every time you think of playing a game. 
